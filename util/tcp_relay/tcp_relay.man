.TH KNEWS 1 "1996"
.SH NAME
tcp_relay \- a tcp relay
.SH SYNOPSIS
.B tcp_relay
-p port [-s] -h host ... remotehost[:port]
.SH DESCRIPTION
For more details, read the source.
.SH OPTIONS
.B -p port
.RS
The port tcp_relay will listen for connections on.
.RE
.B -s
.RS
If this option is given, only a single connection at a time will
be handled.  Otherwise tcp_relay will fork a new copy of itself
for each incoming connection and go on to accept new connections.
.RE
.B -h host
.RS
Connections from this host will be allowed.  Several of these
may be supplied.
.RE
.SH AUTHOR
This software is Copyright 1996 by Karl-Johan Johnsson.
