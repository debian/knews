.TH KNEWSD 1 "1996"
.SH NAME
knewsd \- a tiny nntp demon
.SH SYNOPSIS
.B knewsd
[
.B options
]

.SH DESCRIPTION

Knewsd is intended to be used with knews(1) to read news (in)directly
from the spool directory or even from a mail folder hierarchy.  It reads
NNTP commands from standard input and writes replies to standard output.
The following commands are implemented:

.nf
.B ARTICLE [number]
.B BODY [number]
.B HEAD [number]
.fi
.RS
Return the head and/or body of the current or the specified article.
Message-id lookups are not supported.
.RE

.B GROUP newsgroup
.RS
Enters the given newsgroup.
.RE

.B HELP
.RS
Gives a list of the implemented commands.
.RE

.B LIST [active | newsgroups]
.RS
Lists the active file or the newsgroups file.
.RE

.B NEXT
.RS
Changes the current article pointer to the next article
.RE

.B POST
.RS
Post an article via the specified posting agent.  If none has been
specified, the reply will be '440 Posting not implemented.'
.RE

.B QUIT
.RS
Makes knewsd exit.
.RE

.B STAT number
.RS
Sets the current article pointer.
.RE

.B XOVER range
.RS
Returns the overview data for the specified range of articles.
.RE

Knewsd implements the 'xover' command either by using the overview files if
present, or creating overview records on the fly, or a combination of both
if the overview file is incomplete.  In the latter case, if the \-update
flag is given, knewsd will update the overview files.

.SH OPTIONS
There is no need to spell out the entire option name, it is
sufficient to give a unique prefix, such as \-a for \-active.

.TP
.B \-spool directory
This is the spool directory.  It must be specified, either at compile
time or at run time.
.TP
.B \-active file
This is the active file containing the newsgroups and the high and low
article counts.  If this is not specified, knews must be told not to
try to read the active file.
.TP
.B \-newsgroups file
This is the newsgroups file containing the group descriptions. It's
entirely optional.
.TP
.B \-overview dir
This can be used if the overview files reside in a different directory
hierarchy than the spool directory.
.TP
.B \-update
If this is set, knewsd will (try to) update any incomplete overview files it
encounters.  Not a good idea if you're reading a system wide spool
directory.
.TP
.B \-postingagent agent
If this is set, the 'agent' will be used to post articles.  A possible
agent is 'exec inews -h'.  This command is interpreted by the shell,
and it is important that it is exec'ed, so that knewsd may kill it
on a broken pipe to prevent posting of duplicate or truncated articles.
Knewsd will warn about this on startup.

.SH AUTHOR
This software is Copyright 1995, 1996 by Karl-Johan Johnsson.

.SH "SEE ALSO"
knews(1), inews(1)
