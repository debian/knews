/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

/*
 * Configuration:  all of these can set at runtime instead
 */

/* This is mandatory */
#define SPOOL_DIR		"/var/spool/news"

/* This is advisable */
#define ACTIVE_FILE		"/usr/local/news/active"

/* Entirely voluntary */
/* #define NEWSGROUPS_FILE	"/usr/local/news/newsgroups"*/

/* May be necessary */
/* #define OVERVIEW_DIR		"/var/spool/news/over.view"*/


/* Define this to be able to post.  SHOULD be an exec'ed shell command. */
/* #define POSTING_AGENT	"exec inews -h"*/
