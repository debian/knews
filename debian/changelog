knews (1.0b.1-39) UNRELEASED; urgency=medium

  * Apply X-Style: black.

 -- Colin Watson <cjwatson@debian.org>  Mon, 08 Jul 2024 13:40:14 +0100

knews (1.0b.1-38) unstable; urgency=medium

  * Fix build failures with GCC 14 (closes: #1075114):
    - Don't return without an expression in a function returning int.
    - Fix type of argument to png_destroy_read_struct.

 -- Colin Watson <cjwatson@debian.org>  Wed, 03 Jul 2024 15:25:34 +0100

knews (1.0b.1-37) unstable; urgency=medium

  * Add missing #includes (closes: #1066692).

 -- Colin Watson <cjwatson@debian.org>  Wed, 13 Mar 2024 14:23:02 +0000

knews (1.0b.1-36) unstable; urgency=medium

  * Fix escaping of line-initial apostrophes in man page.
  * Fix various spelling errors.
  * Set Rules-Requires-Root: no.

 -- Colin Watson <cjwatson@debian.org>  Tue, 26 Dec 2023 16:39:07 +0000

knews (1.0b.1-35) unstable; urgency=medium

  * Split up Debian patches.

 -- Colin Watson <cjwatson@debian.org>  Sat, 17 Dec 2022 19:16:44 +0000

knews (1.0b.1-34.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Convert to 3.0 source format.

 -- Bastian Germann <bage@debian.org>  Fri, 02 Dec 2022 19:04:56 +0100

knews (1.0b.1-34) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + knews: Drop versioned constraint on debconf in Depends.

  [ Colin Watson ]
  * Convert several debconf translations to UTF-8.

 -- Colin Watson <cjwatson@debian.org>  Sun, 26 Dec 2021 01:48:40 +0000

knews (1.0b.1-33) unstable; urgency=medium

  [ Colin Watson ]
  * Use debhelper-compat instead of debian/compat.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.

 -- Colin Watson <cjwatson@debian.org>  Mon, 08 Feb 2021 21:47:39 +0000

knews (1.0b.1-32) unstable; urgency=medium

  * Move VCS to salsa.debian.org.
  * Depend on sensible-utils.

 -- Colin Watson <cjwatson@debian.org>  Sat, 27 Oct 2018 21:47:07 +0100

knews (1.0b.1-31) unstable; urgency=medium

  * Use HTTPS for Vcs-* URLs.
  * Build with all hardening options.
  * Upgrade to debhelper v9.
  * Policy version 3.9.6: no changes required.

 -- Colin Watson <cjwatson@debian.org>  Wed, 27 Jan 2016 12:26:37 +0000

knews (1.0b.1-30) unstable; urgency=medium

  * Update Vcs-Browser URL for alioth cgit.
  * Simplify debian/rules using /usr/share/dpkg/buildflags.mk.
  * Run "xmkmf -a" from override_dh_auto_configure rather than
    override_dh_auto_build.
  * Add Italian debconf translation (thanks, Beatrice Torracca; closes:
    #778382).
  * Don't build the compilation date and time into the binary.  This made
    the build unreproducible.

 -- Colin Watson <cjwatson@debian.org>  Sat, 14 Mar 2015 00:23:42 +0000

knews (1.0b.1-29) unstable; urgency=medium

  * Switch to git; add Vcs-* fields.
  * Policy version 3.9.5: no changes required.

 -- Colin Watson <cjwatson@debian.org>  Mon, 27 Jan 2014 22:58:06 +0000

knews (1.0b.1-28) unstable; urgency=low

  * Rebuild in a clean unstable chroot to avoid pulling in experimental's
    libpng15-15.

 -- Colin Watson <cjwatson@debian.org>  Wed, 07 Mar 2012 10:02:21 +0000

knews (1.0b.1-27) unstable; urgency=low

  * Build-depend on libpng-dev rather than libpng12-dev (closes: #662389).

 -- Colin Watson <cjwatson@debian.org>  Mon, 05 Mar 2012 09:44:00 +0000

knews (1.0b.1-26) unstable; urgency=low

  * Adjust debhelper build-dependency to allow for backports.
  * Build-Depend on libjpeg-dev rather than libjpeg62-dev.
  * Fix undersized args array in action_tree_layout.
  * Use strcpy instead of sprintf in fork_editor in the case where neither
    %s nor %i is present in editcmd, to avoid an error under
    -Werror=format-security.
  * Use dpkg-buildflags to set default compiler flags.

 -- Colin Watson <cjwatson@debian.org>  Tue, 27 Sep 2011 01:12:04 +0100

knews (1.0b.1-25) unstable; urgency=low

  [ Nobuhiro Iwamatsu ]
  * Fix build failure with libpng 1.5 (closes: #635949).

 -- Colin Watson <cjwatson@debian.org>  Sat, 30 Jul 2011 09:50:01 +0100

knews (1.0b.1-24) unstable; urgency=low

  * Add Russian debconf translation (thanks, Yuri Kozlov; closes: #513508).
  * Upgrade to debhelper v7.

 -- Colin Watson <cjwatson@debian.org>  Fri, 14 Aug 2009 22:17:20 +0100

knews (1.0b.1-23) unstable; urgency=low

  * Build-depend on xutils-dev rather than xutils (thanks, Daniel Schepler;
    closes: #485206).
  * Use \e rather than \\ in man page, partly because that's more correct
    anyway and partly to avoid cpp backslash-newline translation.
  * Build-depend on x11proto-core-dev rather than x-dev (thanks, Lintian).
  * Refer to /usr/share/common-licenses/GPL-2 in debian/copyright rather
    than plain GPL.
  * Update DEB_BUILD_OPTIONS parsing code from policy 3.8.0.
  * Policy version 3.8.0.

 -- Colin Watson <cjwatson@debian.org>  Sun, 08 Jun 2008 21:24:21 +0100

knews (1.0b.1-22) unstable; urgency=low

  * Add Portuguese debconf translation (thanks, Rui Branco;
    closes: #414144).
  * Update Spanish debconf translation (thanks, Carlos Galisteo de Cabo;
    closes: #424606).
  * Drop source-compatibility with woody.
  * Replace null characters in articles with spaces (thanks, Dick
    Streefland; closes: #227259).
  * Update to menu structure from menu 2.1.35.

 -- Colin Watson <cjwatson@debian.org>  Mon, 12 Nov 2007 11:02:05 +0000

knews (1.0b.1-21) unstable; urgency=low

  * Simplify state machine in config script, avoiding a bashism.
  * Add Vietnamese debconf translation (thanks, Clytie Siddall;
    closes: #314269).
  * Add Swedish debconf translation (thanks, Daniel Nylander;
    closes: #333406).
  * Update Spanish debconf translation (thanks, Carlos Galisteo de Cabo;
    closes: #323382).
  * Use debhelper v4.
  * Update FSF address in debian/copyright.
  * Policy version 3.7.2.

 -- Colin Watson <cjwatson@debian.org>  Sat, 28 Oct 2006 08:52:19 +0100

knews (1.0b.1-20) unstable; urgency=low

  * Add Czech debconf translation (thanks, Miroslav Kure; closes: #291878).

 -- Colin Watson <cjwatson@debian.org>  Mon, 24 Jan 2005 10:40:04 +0000

knews (1.0b.1-19) unstable; urgency=low

  * Depend on debconf | debconf-2.0.

 -- Colin Watson <cjwatson@debian.org>  Thu, 13 Jan 2005 09:04:55 +0000

knews (1.0b.1-18) unstable; urgency=low

  * Build-depend on x-dev, libx11-dev, libxmu-dev, libxpm-dev, libxt-dev,
    libice-dev, and libsm-dev rather than the transitional xlibs-dev
    package.

 -- Colin Watson <cjwatson@debian.org>  Fri, 26 Nov 2004 15:16:02 +0000

knews (1.0b.1-17) unstable; urgency=low

  * Add Japanese debconf translation (thanks, Hideki Yamane;
    closes: #255061).
  * Update Brazilian Portuguese debconf translation (thanks, André Luís
    Lopes; closes: #264208).

 -- Colin Watson <cjwatson@debian.org>  Sat,  7 Aug 2004 17:55:57 +0100

knews (1.0b.1-16) unstable; urgency=low

  * Add Dutch debconf translation (thanks, Frans Pop; closes: #242030).

 -- Colin Watson <cjwatson@debian.org>  Sun,  4 Apr 2004 13:57:42 +0100

knews (1.0b.1-15) unstable; urgency=low

  * Oops; debian/po/POTFILES.in should list templates.master, not templates.

 -- Colin Watson <cjwatson@debian.org>  Sat,  6 Mar 2004 15:15:50 +0000

knews (1.0b.1-14) unstable; urgency=low

  * Add French debconf translation (thanks, Eric Madesclair;
    closes: #235646).

 -- Colin Watson <cjwatson@debian.org>  Mon,  1 Mar 2004 17:42:53 +0000

knews (1.0b.1-13) unstable; urgency=low

  * Convert debconf translation support to po-debconf.
  * Build-depend on libpng12-dev rather than libpng3-dev.
  * Policy version 3.6.1:
    - Drop DEB_BUILD_OPTIONS=debug support; add noopt support.
  * Remove Emacs local variables from this changelog.

 -- Colin Watson <cjwatson@debian.org>  Fri, 20 Feb 2004 11:37:52 +0000

knews (1.0b.1-12) unstable; urgency=low

  * Some new Spanish and Brazilian Portuguese templates from the DDTP.

 -- Colin Watson <cjwatson@debian.org>  Tue, 26 Nov 2002 23:45:41 +0000

knews (1.0b.1-11) unstable; urgency=low

  * Depend on sharutils for uudecode (closes: #158379).
  * Mention knewsd in the package description.

 -- Colin Watson <cjwatson@debian.org>  Tue, 27 Aug 2002 11:07:04 +0100

knews (1.0b.1-10) unstable; urgency=low

  * Make sure XOVER responses always end with CR-LF (thanks, Achim Linder;
    closes: #156632).
  * Install util/knewsd/README as /usr/share/doc/knews/README.knewsd.

 -- Colin Watson <cjwatson@debian.org>  Mon, 26 Aug 2002 12:24:55 +0100

knews (1.0b.1-9) unstable; urgency=low

  * libpng-dev has been renamed to libpng3-dev, so track this in the
    build-dependencies.

 -- Colin Watson <cjwatson@debian.org>  Mon, 29 Jul 2002 11:24:12 +0000

knews (1.0b.1-8) unstable; urgency=low

  * Build against libpng3 (we don't use Qt or GNOME libraries, so this is
    OK).
  * New Danish debconf translation (thanks, Rune B. Broberg;
    closes: #131096).

 -- Colin Watson <cjwatson@debian.org>  Sun, 27 Jan 2002 12:10:16 +0000

knews (1.0b.1-7) unstable; urgency=low

  * New maintainer (closes: #86390).
  * Repackage using debhelper rather than yada.
  * Ask configuration questions using debconf. I've based this on the trn4
    package, which I know to work and which already has a German
    translation.

 -- Colin Watson <cjwatson@debian.org>  Sun, 23 Dec 2001 14:20:10 +0000

knews (1.0b.1-6) unstable; urgency=low

  * Rebuild with yada >= 0.9.8, which works around a bug in dpkg-shlibdeps
    (closes: #92749).
  * debian/png.dpatch: libpng seems to have moved several interfaces to
    PNG_INTERNAL. I've patched knews' png code as best I can; please report
    any bugs that result (closes: #115958).
  * Change maintainer address to packages@qa.debian.org.

 -- Colin Watson <cjwatson@debian.org>  Wed, 17 Oct 2001 20:41:53 +0100

knews (1.0b.1-5) unstable; urgency=medium

  * Use external yada.  Closes: #91755.
  * Conforms to Standards version 3.5.2:
    * Install into /usr rather than /usr/X11R6.
    * Support the `debug' and `nostrip' build options.

 -- Matej Vela <vela@debian.org>  Tue, 27 Mar 2001 06:53:21 +0200

knews (1.0b.1-4) unstable; urgency=medium

  * Make debian/yada executable. (closes: #86551)
  * s/xlib6g-dev/xlibs-dev/ in the build dependencies.

 -- Adrian Bunk <bunk@fs.tum.de>  Thu, 22 Feb 2001 13:28:14 +0100

knews (1.0b.1-3) unstable; urgency=low

  * Recompile with last library dependencies (closes: #67933)
  * Fixed and moved Build-Depends in debian/packages (closes: #83856)
  * Moved Knews in /etc/X11/app-defaults/ (closes: #86276, #78570)
  * Moved man page in /usr/X11R6/man/ (closes: #48147)
  * Orphaned the package

 -- Christophe Le Bars <clebars@debian.org>  Sat, 17 Feb 2001 17:49:14 +0100

knews (1.0b.1-2.1) unstable; urgency=low

  * NMU by Branden Robinson <branden@debian.org>.
  * Recompile against modern xpm4g package to get proper shared library
    dependency and permit this package to be installed with forthcoming
    XFree86 4.x packages, which include libXpm.
  * debian/control: added Build-Depends

 -- Branden Robinson <branden@debian.org>  Mon, 23 Oct 2000 12:17:04 -0500

knews (1.0b.1-2) unstable; urgency=low

  * Recompiled against libjpeg62 (closes: #43349)
  * Switched /usr/doc/ -> /usr/share/doc/

 -- Christophe Le Bars <clebars@debian.org>  Mon, 27 Sep 1999 02:05:00 +0200

knews (1.0b.1-1) unstable; urgency=low

  * New upstream release with pristine sources
  * Converted package management to YADA
  * Partially upgraded to standards version 3.0.0:
     - FHS-compliance, with the notable exception of the
     /usr/doc/ -> /usr/share/doc/ switch.
  * Fixed a bashism in postinst (closes: #30856)

 -- Christophe Le Bars <clebars@debian.org>  Sun,  8 Aug 1999 15:39:53 +0200

knews (1.0b.0-3) unstable; urgency=low

  * Recompiled against libpng2 (Fixes: Bug#26925)
  * Corrected knews to directly use /etc/news/server

 -- Christophe Le Bars <clebars@debian.org>  Tue, 29 Sep 1998 01:22:03 +0200

knews (1.0b.0-2) frozen unstable; urgency=low

  * Updated standards-version
  * Changed FSF address
  * Corrected postinst to use /etc/mailname
  * Corrected postinst to use tempfile
  * Fixed knews to use /usr/bin/sensible-editor

 -- Christophe Le Bars <clebars@debian.org>  Fri, 10 Apr 1998 18:09:44 +0200

knews (1.0b.0-1) unstable; urgency=low

  * New upstream release with pristine sources
  * Changed distribution (now GPL)

 -- Christophe Le Bars <clebars@debian.org>  Mon,  9 Feb 1998 00:40:42 +0000

knews (0.9.8-10) unstable; urgency=low

  * Fixed \r\n endline problem when Knews calls inews and sendmail
  (Mark Baker patch) (Bug#9317)

 -- Christophe Le Bars <clebars@debian.org>  Mon, 15 Dec 1997 01:58:52 +0100

knews (0.9.8-9) unstable; urgency=low

  * Built with libc6

 -- Christophe Le Bars <clebars@debian.org>  Sun, 28 Sep 1997 00:43:36 +0200

knews (0.9.8-8) unstable; urgency=low

  * Fixed distribution problem in changelog file
  * Fixed postinst problem (Bug#12313)

 -- Christophe Le Bars <clebars@debian.org>  Wed, 10 Sep 1997 00:06:27 +0200

knews (0.9.8-7) stable-non-free non-free; urgency=medium

  * Fixed a security problem in the postinst script (Bug#1177)
  * Added a menu entry
  * Changed distribution

 -- Christophe Le Bars <clebars@debian.org>  Sun, 10 Aug 1997 19:27:54 +0200

knews (0.9.8-6) unstable; urgency=low

  * Set the domain name with /etc/news/whoami (Bug#10844) (Bug#10369)

 -- Christophe Le Bars <clebars@debian.org>  Sun, 29 Jun 1997 20:18:10 +0200

knews (0.9.8-5) frozen unstable; urgency=medium

  * Included many bugfixes from upstream (Bug#6872)

 -- Christophe Le Bars <clebars@debian.org>  Tue, 6 May 1997 20:16:42 +0200

knews (0.9.8-4) frozen unstable; urgency=low

  * New maintainer
  * Updated to new standards

 -- Christophe Le Bars <clebars@debian.org>  Tue, 8 Apr 1997 23:00:27 +0200

knews (0.9.8-3) unstable; urgency=low

  * Corrected maintainer address

 -- Michael Alan Dorman <mdorman@debian.org>  Mon, 23 Sep 1996 12:32:03 -0400

knews (0.9.8-2) unstable; urgency=low

  * Accomodate the fact that dpkg-source doesn't properly preserve
    permissions on scripts when extracting package.

 -- Michael Alan Dorman <mdorman@calder.med.miami.edu>  Mon, 23 Sep 1996 11:28:37 -0400

knews (0.9.8-1) unstable; urgency=low

  * New upstream version
  * New packaging format

 -- Michael Alan Dorman <mdorman@calder.med.miami.edu>  Mon, 16 Sep 1996 14:39:40 -0400
