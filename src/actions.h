/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void action_tree_up(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_down(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_left(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_right(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_down_right(Widget, XEvent*, String*, Cardinal*);
extern void action_list_up(Widget, XEvent*, String*, Cardinal*);
extern void action_list_down(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_or_list_up(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_or_list_down(Widget, XEvent*, String*, Cardinal*);
extern void action_exit_mode(Widget, XEvent*, String*, Cardinal*);
extern void action_enter_mode(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_left_or_exit_mode(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_right_or_enter_mode(Widget, XEvent*,
					    String*, Cardinal*);
extern void action_goto_next_hot(Widget, XEvent*, String*, Cardinal*);
extern void action_view_thread(Widget, XEvent*, String*, Cardinal*);
extern void action_change_size(Widget, XEvent*, String*, Cardinal*);
extern void action_popup_find_group(Widget, XEvent*, String*, Cardinal*);
extern void action_do_the_right_thing(Widget, XEvent*, String*, Cardinal*);
extern void action_tree_layout(Widget, XEvent*, String*, Cardinal*);
