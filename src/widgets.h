/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct main_widgets {
    Widget	shell;
    Widget	second_shell;
    Widget	top_manager;
    Widget	top_layout;
    Widget	arttree;
    Widget	group_list;
    Widget	thread_list;
    Widget	knapp[12];
    Widget	text;
};

extern struct main_widgets main_widgets;

extern void create_main_widgets(void);
extern void setNewsModeDisconnected(void);
extern void setNewsModeConnected(void);
extern void setNewsModeGroup(int);
extern void setNewsModeThread(void);
extern void setNewsModeAllgroups(regex_t*);
extern void setNewsModeNewgroups(void);
extern void update_subj_entry(SUBJECT*);
extern void update_group_entry(GROUP*);
extern void purge_hot(Pixmap);
extern void set_message(char*, int);
