/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void       arttree_tag_callback(Widget, XtPointer, XtPointer);
extern void       thread_list_tag_callback(Widget, XtPointer, XtPointer);
extern void       clear_tagged_articles(void);
extern ARTICLE  **get_tagged_articles(void);
extern long       no_tagged_articles(void);
extern void       mark_tagged_articles(ARTICLE*);
extern void       tag_hot_articles(void);
extern void	  untag_article(ARTICLE*);
extern void       history_push(ARTICLE*);
extern ARTICLE   *history_pop(void);
extern ARTICLE   *history_peek(void);
extern void       clear_history(void);
extern void       action_tag_thread(Widget, XEvent*, String*, Cardinal*);
extern void       action_tag_subject(Widget, XEvent*, String*, Cardinal*);
extern void       action_untag_thread(Widget, XEvent*, String*, Cardinal*);
extern void       action_untag_subject(Widget, XEvent*, String*, Cardinal*);
