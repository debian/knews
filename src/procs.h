/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void knapp0_callback(Widget, XtPointer, XtPointer);
extern void knapp1_callback(Widget, XtPointer, XtPointer);
extern void knapp2_callback(Widget, XtPointer, XtPointer);
extern void knapp5_callback(Widget, XtPointer, XtPointer);
extern void knapp6_callback(Widget, XtPointer, XtPointer);
extern void knapp7_callback(Widget, XtPointer, XtPointer);
extern void knapp8_callback(Widget, XtPointer, XtPointer);
extern void knapp10_callback(Widget, XtPointer, XtPointer);
extern void knapp11_callback(Widget, XtPointer, XtPointer);
extern void thread_list_sel_callback(Widget, XtPointer, XtPointer);
extern void thread_list_callback(Widget, XtPointer, XtPointer);
extern void arttree_sel_callback(Widget, XtPointer, XtPointer);
extern void group_list_callback(Widget, XtPointer, XtPointer);
extern void group_list_sel_callback(Widget, XtPointer, XtPointer);
extern void group_list_dnd_callback(Widget, XtPointer, XtPointer);
extern void delete_window_callback(Widget, XtPointer, XtPointer);
extern void sash_callback(Widget, XtPointer, XtPointer);
