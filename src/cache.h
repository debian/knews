/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void		 cache_enter_group(void);
extern void		 cache_leave_group(void);
extern FILE		*cache_get_file(long);
extern struct SERVER	*cache_get_server(long, int);
extern void		 cache_fetch_done(long);
extern void		 cache_fetch_failed(long);
extern int		 cache_todo(void);
extern void		 popup_cache_stats(void);
