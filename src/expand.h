/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct MimeArg;

extern char	*regexp_escape_string(char*, int);
extern char	*expand_view_command(const char*, char*, char*,
				     struct MimeArg*, int, int);
extern char	*expand_path(char*);
