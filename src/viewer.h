/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void	do_viewer(char*, char*, char*, char*, char*, long);
extern void	destroy_pixmap_callback(Widget, XtPointer, XtPointer);
