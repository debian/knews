/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern int	 read_article(ARTICLE*, int, long*, char**);
extern char	*do_mime(ARTICLE*, struct SERVER*,
			 char *, int, char*, int, char**);
extern void	 action_read_article(Widget, XEvent*, String*, Cardinal*);
extern void	 action_mime_hack(Widget, XEvent*, String*, Cardinal*);
