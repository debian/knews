/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern char	thread_ahead_char(GROUP*);
extern void	thread_ahead_shutdown(void);
extern int	thread_ahead_check(GROUP*);
extern void	thread_ahead_leave_group(GROUP*);
extern void	thread_ahead_init(void);
extern int	thread_ahead_todo(void);

extern void	action_schedule_thread_ahead(Widget w, XEvent *event,
					     String *params,
					     Cardinal *no_params);

