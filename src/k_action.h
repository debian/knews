/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void action_kill_append(Widget, XEvent*, String*, Cardinal*);
extern void action_kill_prepend(Widget, XEvent*, String*, Cardinal*);
extern void action_popup_kill(Widget, XEvent*, String*, Cardinal*);
