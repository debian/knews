/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct stat;

extern int	open_mkdir(char*, int, int);
extern int	open_expand(char*, int, int);
extern FILE    *fopen_mkdir(char*, char*, int);
extern FILE    *fopen_expand(char*, char*, int);
extern int	unlink_expand(char*);
extern int	chdir_mkdir(char*);
extern int      create_temp_fd(char**);
extern FILE    *create_temp_file(char**);
extern char    *snarf_file(int, long*);
extern int	writen(int, char*, long);
