/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void update_misc_menu(NewsMode);
extern void create_misc_menu1(Widget);
extern void create_misc_menu2(Widget);
extern void popdown_msgid_dialogue(void);
extern void action_mark_read_article(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_subject(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_thread(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_subthread(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_tagged(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_all(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_to_current(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_non_tagged(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_read_cold(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_article(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_subject(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_thread(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_subthread(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_tagged(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_all(Widget, XEvent*, String*, Cardinal*);
extern void action_mark_unread_killed(Widget, XEvent*, String*, Cardinal*);
extern void action_clear_tagged(Widget, XEvent*, String*, Cardinal*);
extern void action_catchup(Widget, XEvent*, String*, Cardinal*);
extern void action_subscribe(Widget, XEvent*, String*, Cardinal*);
extern void action_unsubscribe(Widget, XEvent*, String*, Cardinal*);
extern void action_tag_hot(Widget, XEvent*, String*, Cardinal*);
