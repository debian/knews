/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

typedef void*	(*DecodeInit)(void);
typedef long	(*DecodeDecode)(void*, char*, long, int, XChar2b**);
typedef void	(*DecodeEnd)(void*);

typedef struct DecodeFuncs {
    DecodeInit		init;
    DecodeDecode	decode;
    DecodeEnd		end;
} DecodeFuncs;

extern const DecodeFuncs	*get_decode_funcs(char*);
