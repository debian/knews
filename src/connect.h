/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void	connect_server(void);
extern int	reconnect_server(int);
extern void	popup_connect_dialogue(void);
