/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern const char	base64_alpha[64];
extern const char	inv_base64_alpha[256];

#define BASE64_ERROR		1u
#define BASE64_TRAILING_GARBAGE	2u

#define UUE_NO_BEGIN		1u
#define UUE_NO_END		2u
#define UUE_ERROR		4u

typedef struct {
    unsigned char	state;
    unsigned char	err;
} UueContext;

typedef struct {
    unsigned long	data;
    unsigned int	n;
    unsigned char	end;
    unsigned char	err;
    unsigned char	trailing_garbage;
} B64Context;

extern long	decode_qp(char*, char*, long, int*, int);
extern long	decode_base64(B64Context*, char*, char*, long);
extern int	base64_status(B64Context*);
extern long	decode_uue(UueContext*, char*, char*, long);
extern int	uue_status(UueContext*);
