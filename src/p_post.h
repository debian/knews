/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct PostContext;

extern int	 post_article(FILE*);
extern int	 post_to_agent(char*, FILE*);
extern FILE	*dump_art_to_file(struct PostContext*);
