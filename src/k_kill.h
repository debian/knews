/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

struct KILL_NODE;

typedef long	(*KillFunc)(struct KILL_NODE*, ARTICLE*, SUBJECT*);

extern const KillFunc	kill_funcs[4][4];
extern const KillFunc	hot_funcs[4][4];
