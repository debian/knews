/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

typedef struct {
    unsigned char	pixel;
    unsigned char	r;
    unsigned char	g;
    unsigned char	b;
} CMAP_ENTRY;

extern CMAP_ENTRY	*cmap;
extern int		 cmap_size;

extern void	color_init(Display*);
extern void	alloc_colors(void);
extern Pixmap	put_8_image(unsigned char*, long, long);
extern Pixmap	put_24_image(unsigned char*, long, long);
extern Pixmap	put_grey_image(unsigned char*, long, long);
extern Pixmap	put_cmapped_image(unsigned char*, long, long,
				  CMAP_ENTRY*, unsigned int);
extern Pixel	get_closest_color(XColor*);
