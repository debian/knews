/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#include "global.h"
#include "expand.h"
#include "k_I.h"
#include "k_action.h"
#include "k_file.h"
#include "k_node.h"
#include "newsrc.h"
#include "util.h"
#include "widgets.h"
#include "xutil.h"

static const char *scope_str[] =
{"article",	"subject",	"thread",	"subthread"};
static const char *field_str[] =
{"Message-Id",	"Subject",	"From",		"Xref"};

static void add_kill(String	*params,
		     int	 no_params,
		     int	 append,
		     int	 global_kill_file)
{
    KILL_FILE	*file;
    char	buffer[128];
    int		scope, field, hot;
    char	*expr  = NULL;
    char	*group = NULL;

    if (global.busy || (global.mode != NewsModeGroup &&
			global.mode != NewsModeThread)) {
	XBell(display, 0);
	return;
    }

    if (no_params != 2 && no_params != 3) {
	if (append)
	    set_message("kill-append() called with "
			"wrong number of arguments!", True);
	else
	    set_message("kill-prepend() called with "
			"wrong number of arguments!", True);
	return;
    }

    if (case_lstrcmp(params[0], "from") == 0) {
	field = KillFieldFrom;
	if (global.curr_art)
	    expr = regexp_escape_string(global.curr_art->from, True);
    } else if (case_lstrcmp(params[0], "subject") == 0) {
	field = KillFieldSubject;
	if (global.curr_art)
	    expr = regexp_escape_string(global.curr_art->subject->subject,
					True);
	else if (global.curr_subj)
	    expr = regexp_escape_string(global.curr_subj->subject, True);
    } else if (case_lstrcmp(params[0], "message-id") == 0) {
	field = KillFieldMsgid;
	if (global.curr_art) {
	    expr = XtMalloc(global.curr_art->hash_len + 4);
	    sprintf(expr, "<%s>", global.curr_art->msgid);
	}
    } else {
	sprintf(buffer, "kill-%s(): unknown field: ",
		append ? "append" : "prepend");
	strncat(buffer, params[0], 40);
	set_message(buffer, True);
	return;
    }

    if (case_lstrcmp(params[1], "article") == 0)
	scope = KillScopeArticle;
    else if (case_lstrcmp(params[1], "thread") == 0)
	scope = KillScopeThread;
    else if (case_lstrcmp(params[1], "subthread") == 0)
	scope = KillScopeSubthread;
    else if (case_lstrcmp(params[1], "subject") == 0)
	scope = KillScopeSubject;
    else {
	sprintf(buffer, "kill-%s(): unknown scope: ",
		append ? "append" : "prepend");
	strncat(buffer, params[1], 40);
	set_message(buffer, True);
	XtFree(expr);
	return;
    }

    if (!expr) {
	set_message("No selected article!", True);
	return;
    }

    if (!global_kill_file)
	file = get_kill_file(global.curr_group);
    else {
	group = ".";
	file = get_kill_file(NULL);
    }

    hot = no_params == 3;
    if (add_kill_node(file, append, field, scope, hot,
		      hot ? params[2] : NULL, expr, group)) {
	sprintf(buffer, "%s %s-scope %s %s entry to %skill file.",
		append ? "Appended" : "Prepended",
		scope_str[scope], field_str[field], hot ? "hot" : "kill",
		global_kill_file ? "global " : "");
	set_message(buffer, False);
    }
    XtFree(expr);
}

void action_kill_append(Widget    w,
			XEvent   *event,
			String   *params,
			Cardinal *no_params)
{
    add_kill(params, *no_params, True, False);
}

void action_kill_prepend(Widget    w,
			 XEvent   *event,
			 String   *params,
			 Cardinal *no_params)
{
    add_kill(params, *no_params, False, False);
}

void action_kill_append_global(Widget    w,
			       XEvent   *event,
			       String   *params,
			       Cardinal *no_params)
{
    add_kill(params, *no_params, True, True);
}

void action_kill_prepend_global(Widget    w,
				XEvent   *event,
				String   *params,
				Cardinal *no_params)
{
    add_kill(params, *no_params, False, True);
}

void action_popup_kill(Widget      w,
		       XEvent     *event,
		       String     *params,
		       Cardinal   *no_params)
{
    GROUP	*group;

    if (global.busy || global.mode == NewsModeDisconnected)
	return;

    if (*no_params != 1)
	group = NULL;
    else {
	group = find_group(params[0]);
	if (!group) {
	    set_message("No such group!", True);
	    return;
	}
    }

    kill_edit_popup(group);
}
