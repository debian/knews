/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

typedef struct {
    XFontStruct		*body_font;
    XFontStruct		*quote_font;
    XFontStruct		*header_font;
    XFontStruct		*list_font;
    XFontStruct		*tree_font;
    const struct DecodeFuncs	*funcs;
    int			head_enc_hack;
} MimeFont;

extern MimeFont	*default_font;
extern MimeFont	*ascii_font;

extern void	 init_fonts(Widget);
extern MimeFont	*get_font(char*);
extern void	 font_enter_group(void);
