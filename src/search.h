/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

extern void popup_search(void);
extern void popdown_search(void);
extern void popup_find_group(void);
extern void popdown_find_group(void);
extern void set_busy_search(int);
