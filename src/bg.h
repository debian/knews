/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

typedef int	(*BgProc)(struct SERVER*);

extern void		 bg_nudge(BgProc);
extern void		 bg_shutdown(void);
extern GROUP		*bg_in_group(long*, long*, long*);
extern void		 bg_start_group(GROUP*);
extern void		 close_bg_server(void);
