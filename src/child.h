/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

typedef void (*ChildCallback)(void*, int, char*);

extern void	 init_child_contexts(void);
extern void	 suspend_child_contexts(void);
extern void	 resume_child_contexts(void);
extern pid_t	 fork_nicely(void*, ChildCallback, int);
extern pid_t	 wait_for_pid(pid_t, int*, char*);
extern char	*signal_string(int);

extern void	 block_sighup(void);
extern void	 unblock_sighup(void);

#define STDERR_BUFFLEN	1024
