/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuG_h
#define MenuG_h

#ifndef XtCLabel
#define XtCLabel "Label"
#endif

#ifndef XtNlabel
#define XtNlabel "label"
#endif
#ifndef XtNpostPopdownCallback
#define XtNpostPopdownCallback "postPopdownCallback"
#endif

typedef struct MenuGadgetClassRec*  MenuGadgetClass;
typedef struct MenuGadgetRec*       MenuGadget;

extern WidgetClass menuGadgetClass;

#endif /* MenuG_h */
