/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ScrBarP_h
#define ScrBarP_h

#include "ScrBar.h"
#include "Util.h"
#include "ShadowP.h"

typedef enum {
    ScrBarModeNone,
    ScrBarModeForwardLine,
    ScrBarModeBackwardLine,
    ScrBarModeForwardPage,
    ScrBarModeBackwardPage,
    ScrBarModeContinuous
} ScrBarMode;

typedef struct {
    XtPointer	extension;
} ScrBarClassPart;

typedef struct ScrBarClassRec {
    CoreClassPart       core_class;
    ShadowClassPart	shadow_class;
    ScrBarClassPart     scrbar_class;
} ScrBarClassRec;

extern ScrBarClassRec scrBarClassRec;

typedef struct {
    long		canvas_length;
    long		slider_length;
    long		slider_position;
    long		step_size;
    XtCallbackList      scroll_callback;
    int			initial_delay;
    int			minimum_delay;
    int			decay;
    Dimension		minimum_thumb;
    Dimension		thickness;
    Boolean		vertical;
    Boolean		push_thumb;
    Boolean		allow_off;
    Boolean		sync_scroll;
    /* private data */
    GC			bg_gc;
    Position		c1, c2, c3, c4;
    Position		init_ptr_pos;
    Position		init_slider_pos;
    ScrBarMode		mode;
    XtIntervalId	timer;
    int			this_delay;
} ScrBarPart;

typedef struct ScrBarRec {
    CorePart    core;
    ShadowPart	shadow;
    ScrBarPart  scrbar;
} ScrBarRec;

#endif /* ScrBarP_h */
