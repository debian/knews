/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Knapp_h
#define Knapp_h

#ifndef XtCInternalHeight
#define XtCInternalHeight "InternalHeight"
#endif
#ifndef XtCLeftMargin
#define XtCLeftMargin "LeftMargin"
#endif
#ifndef XtCRightMargin
#define XtCRightMargin "RightMargin"
#endif
#ifndef XtCResizable
#define XtCResizable "Resizable"
#endif

#ifndef XtNleftMargin
#define XtNleftMargin "leftMargin"
#endif
#ifndef XtNrightMargin
#define XtNrightMargin "rightMargin"
#endif
#ifndef XtNresizable
#define XtNresizable "resizable"
#endif

typedef struct KnappClassRec*	KnappWidgetClass;
typedef struct KnappRec*	KnappWidget;

extern WidgetClass knappWidgetClass;

extern void	KnappSetLabel(Widget, char*);
extern void	KnappSetActive(Widget, int);
extern void	KnappSetLabelNo(Widget, int, int);
extern int	KnappGetLabelNo(Widget);
extern void	KnappSetSensitive(Widget, int);

#endif /* Knapp_h */
