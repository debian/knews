/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef FileSel_h
#define FileSel_h

#ifndef XtCDirectory
#define XtCDirectory "Directory"
#endif
#ifndef XtCShowDotFiles
#define XtCShowDotFiles "ShowDotFiles"
#endif

#ifndef XtNbusyCursor
#define XtNbusyCursor "busyCursor"
#endif
#ifndef XtNcursor
#define XtNcursor "cursor"
#endif
#ifndef XtNdirectory
#define XtNdirectory "directory"
#endif
#ifndef XtNshowDotFiles
#define XtNshowDotFiles "showDotFiles"
#endif

typedef struct FileSelClassRec*		FileSelWidgetClass;
typedef struct FileSelRec*		FileSelWidget;

extern WidgetClass fileSelWidgetClass;

#endif /* FileSel_h */
