/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Scrollable_h
#define Scrollable_h

#ifndef XtCScrBar
#define XtCScrBar "ScrBar"
#endif

#ifndef XtNhBar
#define XtNhBar "hBar"
#endif
#ifndef XtNvBar
#define XtNvBar "vBar"
#endif

typedef struct ScrollableClassRec*	ScrollableWidgetClass;
typedef struct ScrollableRec*		ScrollableWidget;

extern WidgetClass scrollableWidgetClass;

extern void	ScrollableSetHBar(Widget, Widget);
extern void	ScrollableSetVBar(Widget, Widget);
extern void	ScrollableSuspend(Widget);
extern void	ScrollableResume(Widget);
extern void	ScrollableSetHPos(Widget, long);
extern void	ScrollableSetVPos(Widget, long);
extern long	ScrollableGetVPos(Widget);
extern long	ScrollableGetVShown(Widget);
extern long	ScrollableGetVSize(Widget);
extern void	ScrollablePage(Widget, float);

#endif /* Scrollable_h */
