/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Dialogue_h
#define Dialogue_h

#ifndef XtCMessage
#define XtCMessage "Message"
#endif
#ifndef XtCBuffer
#define XtCBuffer "Buffer"
#endif
#ifndef XtCLabel
#define XtCLabel "Label"
#endif

#ifndef XtNmessage
#define XtNmessage "message"
#endif
#ifndef XtNbuffer
#define XtNbuffer "buffer"
#endif
#ifndef XtNleftLabel
#define XtNleftLabel "leftLabel"
#endif
#ifndef XtNmiddleLabel
#define XtNmiddleLabel "middleLabel"
#endif
#ifndef XtNrightLabel
#define XtNrightLabel "rightLabel"
#endif

typedef struct DialogueClassRec*  DialogueWidgetClass;
typedef struct DialogueRec*       DialogueWidget;

extern WidgetClass dialogueWidgetClass;

typedef enum {
    DialogueReplyLeft,
    DialogueReplyMiddle,
    DialogueReplyRight,
    DialogueReplyEnter,
    DialogueReplyTab,
    DialogueReplyClose
} DialogueReply;

typedef struct {
    DialogueReply	reply;
    String		buffer;
} DialogueReport;

extern Widget	DialogueGetTextField(Widget);

#endif /* Dialogue_h */
