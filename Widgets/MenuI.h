/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuI_h
#define MenuI_h

extern int	NotifyMenu(Widget);
extern int	NotifyMenuShell(Widget);
extern void	PopdownMenu(Widget);
extern void	PopdownMenuShell(Widget);
extern int	PostNotifyMenu(Widget);
extern int	PostNotifyMenuShell(Widget);
extern void	SetActiveMenu(Widget, int);
extern void	SetActiveMenuShell(Widget, int);

#endif /* MenuI_h */
