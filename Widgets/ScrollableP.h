/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ScrollableP_h
#define ScrollableP_h

#include "Scrollable.h"
#include "ScrBar.h"
#include "ShadowP.h"

typedef void	(*ScrollableSetPosProc)(ScrollableWidget, long);
typedef void	(*ScrollableSuspendHook)(ScrollableWidget);

#define XtInheritScrollableSetPos	((ScrollableSetPosProc)_XtInherit)
#define XtInheritScrollableSuspendHook	((ScrollableSuspendHook)_XtInherit)

typedef struct {
    ScrollableSetPosProc	set_hpos;
    ScrollableSetPosProc	set_vpos;
    ScrollableSuspendHook	suspend_hook;
    XtPointer			extension;
} ScrollableClassPart;

typedef struct ScrollableClassRec {
    CoreClassPart	core_class;
    ShadowClassPart	shadow_class;
    ScrollableClassPart	scrollable_class;
} ScrollableClassRec;

extern ScrollableClassRec	scrollableClassRec;

typedef struct {
    Widget		h_bar;
    Widget		v_bar;
    /* private */
    long		pos_x;
    long		shown_x;
    long		width;
    long		pos_y;
    long		shown_y;
    long		height;
    int			suspended;
} ScrollablePart;

typedef struct ScrollableRec {
    CorePart		core;
    ShadowPart		shadow;
    ScrollablePart	scrollable;
} ScrollableRec;

extern void	ScrollableFitHBar(ScrollableWidget);
extern void	ScrollableFitVBar(ScrollableWidget);
extern void	ScrollableHFromGeometry(ScrollableWidget);
extern void	ScrollableVFromGeometry(ScrollableWidget);

#endif /* ScrollableP_h */
