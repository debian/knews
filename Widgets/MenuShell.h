/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MenuShell_h
#define MenuShell_h

typedef struct MenuShellClassRec*	MenuShellWidgetClass;
typedef struct MenuShellRec*		MenuShellWidget;

extern WidgetClass menuShellWidgetClass;

#endif /* MenuShell_h */
