/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef StringGP_h
#define StringGP_h

#include "StringG.h"
#include "MenuGP.h"

typedef struct {
    XtPointer		extension;
} StringGadgetClassPart;

typedef struct StringGadgetClassRec {
    RectObjClassPart		rect_class;
    MenuGadgetClassPart		menu_g_class;
    StringGadgetClassPart	string_g_class;
} StringGadgetClassRec;

extern StringGadgetClassRec stringGadgetClassRec;

typedef struct {
    String	command;	/* user data */
    XFontStruct	*font;
    Pixel	foreground_pixel;
    Dimension	left_margin;
    Dimension	right_margin;
    Dimension	internal_height;
    Dimension	shadow_width;
    /* private data */
    GC		default_gc;
    GC		gray_gc;
    Pixmap	stipple;
} StringGadgetPart;

typedef struct StringGadgetRec {
    ObjectPart		object;
    RectObjPart		rectangle;
    MenuGadgetPart	menu_g;
    StringGadgetPart	string_g;
} StringGadgetRec;

#endif /* StringGP_h */
