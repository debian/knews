/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef PullRight_h
#define PullRight_h

#ifndef XtCMenuName
#define XtCMenuName "MenuName"
#endif
#ifndef XtCArrowSize
#define XtCArrowSize "ArrowSize"
#endif
#ifndef XtCArrowOffset
#define XtCArrowOffset "ArrowOffset"
#endif
#ifndef XtCMenuName
#define XtCMenuName "MenuName"
#endif

#ifndef XtNmenuName
#define XtNmenuName "menuName"
#endif
#ifndef XtNarrowSize
#define XtNarrowSize "arrowSize"
#endif
#ifndef XtNarrowOffset
#define XtNarrowOffset "arrowOffset"
#endif
#ifndef XtNarrowShadowWidth
#define XtNarrowShadowWidth "arrowShadowWidth"
#endif
#ifndef XtNmenuName
#define XtNmenuName "menuName"
#endif

typedef struct PullRightGadgetClassRec*		PullRightGadgetClass;
typedef struct PullRightGadgetRec*		PullRightGadget;

extern WidgetClass pullRightGadgetClass;

#endif /* PullRight_h */
