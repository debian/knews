/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Message_h
#define Message_h

#ifndef XtCBuffer
#define XtCBuffer "Buffer"
#endif
#ifndef XtCCenter
#define XtCCenter "Center"
#endif
#ifndef XtCInternalHeight
#define XtCInternalHeight "InternalHeight"
#endif
#ifndef XtCInternalWidth
#define XtCInternalWidth "InternalWidth"
#endif
#ifndef XtCPreferredChars
#define XtCPreferredChars "PreferredChars"
#endif

#ifndef XtNbuffer
#define XtNbuffer "buffer"
#endif
#ifndef XtNcenter
#define XtNcenter "center"
#endif
#ifndef XtNpreferredChars
#define XtNpreferredChars "preferredChars"
#endif


typedef struct MessageClassRec*  MessageWidgetClass;
typedef struct MessageRec*       MessageWidget;

extern WidgetClass messageWidgetClass;

extern void MessageSetAndRedraw(Widget, char*, int);

#endif /* Message_h */
