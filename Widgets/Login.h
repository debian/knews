/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Login_h
#define Login_h

#ifndef XtCMessage
#define XtCMessage "Message"
#endif

#ifndef XtNmessage
#define XtNmessage "message"
#endif
#ifndef XtNuserNameBuffer
#define XtNuserNameBuffer "userNameBuffer"
#endif
#ifndef XtNpassWordBuffer
#define XtNpassWordBuffer "passWordBuffer"
#endif
#ifndef XtNuserNameLabel
#define XtNuserNameLabel "userNameLabel"
#endif
#ifndef XtNpassWordLabel
#define XtNpassWordLabel "passWordLabel"
#endif
#ifndef XtNleftLabel
#define XtNleftLabel "leftLabel"
#endif
#ifndef XtNmiddleLabel
#define XtNmiddleLabel "middleLabel"
#endif
#ifndef XtNrightLabel
#define XtNrightLabel "rightLabel"
#endif
#ifndef XtNfieldWidth
#define XtNfieldWidth "fieldWidth"
#endif

typedef struct LoginClassRec*	LoginWidgetClass;
typedef struct LoginRec*	LoginWidget;

extern WidgetClass loginWidgetClass;

typedef enum {
    LoginReplyLeft,
    LoginReplyMiddle,
    LoginReplyRight,
    LoginReplyEnter,
    LoginReplyClose
} LoginReply;

typedef struct {
    LoginReply	reply;
    char	*username;
    char	*password;
} LoginReport;

#endif /* Login_h */
