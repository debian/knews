/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ArtTreeNode_h
#define ArtTreeNode_h

typedef struct art_tree_node {
    struct art_tree_private	*hook;
    struct art_tree_node	*parent;
    struct art_tree_node	*sibling;
    struct art_tree_node	*child1;
    char			*label;
} ART_TREE_NODE;

#endif /* ArtTreeNode_h */
