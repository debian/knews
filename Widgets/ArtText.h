/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ArtText_h
#define ArtText_h

#ifndef XtCMargin
#define XtCMargin "Margin"
#endif
#ifndef XtCPreferredLines
#define XtCPreferredLines "PreferredLines"
#endif
#ifndef XtCPreferredColumns
#define XtCPreferredColumns "PreferredColumns"
#endif
#ifndef XtCWrapLines
#define XtCWrapLines "WrapLines"
#endif

#ifndef XtNhighlightColor
#define XtNhighlightColor "highlightColor"
#endif
#ifndef XtNseparatorMargin
#define XtNseparatorMargin "separatorMargin"
#endif
#ifndef XtNpreferredLines
#define XtNpreferredLines "preferredLines"
#endif
#ifndef XtNpreferredColumns
#define XtNpreferredColumns "preferredColumns"
#endif
#ifndef XtNurlCallback
#define XtNurlCallback "urlCallback"
#endif
#ifndef XtNwrapLines
#define XtNwrapLines "wrapLines"
#endif
#ifndef XtNimageMargin
#define XtNimageMargin "imageMargin"
#endif
#ifndef XtNmargin
#define XtNmargin "margin"
#endif

typedef struct ArtTextClassRec*		ArtTextWidgetClass;
typedef struct ArtTextRec*		ArtTextWidget;

extern WidgetClass artTextWidgetClass;

typedef struct {
    const char	*line;
    int		sel_ok;
    long	start;
    long	stop;
} ArtTextUrlReport;

extern void ArtTextRot13(Widget);
extern void ArtTextClearLines(Widget);
extern void ArtTextAddLine(Widget, const char*, XFontStruct*, Pixel);
extern void ArtTextAddWLine(Widget, XChar2b*, long, XFontStruct*, Pixel);
extern void ArtTextAppendToLast(Widget, const char*);
extern void ArtTextWAppendToLast(Widget, XChar2b*, long);
extern void ArtTextAddSelected(Widget, const char*,
			       XFontStruct*, Pixel, long, long);
extern void ArtTextAddSeparator(Widget, int, int);
extern void ArtTextAddClickable(Widget, const char*, XFontStruct*, Pixel,
				XtCallbackProc, void*);
extern void ArtTextAddImage(Widget, Pixmap, int, int,
			    XtCallbackProc, XtPointer);
extern int  ArtTextDumpToFile(Widget, FILE*);
extern void ArtTextAllocLines(Widget, long);

#endif /* ArtText_h */
