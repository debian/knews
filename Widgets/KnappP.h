/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef KnappP_h
#define KnappP_h

#include "Knapp.h"
#include "Util.h"
#include "ShadowP.h"

typedef struct {
    XtPointer	empty;
} KnappClassPart;

typedef struct KnappClassRec {
    CoreClassPart	core_class;
    ShadowClassPart	shadow_class;
    KnappClassPart	knapp_class;
} KnappClassRec;

extern KnappClassRec knappClassRec;

typedef struct {
    Pixel		foreground_pixel;
    XFontStruct		*font;
    String		label;
    XtCallbackList      callback;
    Dimension		internal_height;
    Dimension		left_margin;
    Dimension		right_margin;
    JustifyType		justify;
    Boolean		resizable;
    /* private data */
    GC			default_gc;
    GC			gray_gc;
    Pixmap		stipple;
    char		**labels;
    short		no_labels;
    short		label_x;
    short		label_width;
    short		max_width;
    short		label_no;
    Boolean		set;
    Boolean		calling_callbacks;
    Boolean		active;
} KnappPart;

typedef struct KnappRec {
    CorePart	core;
    ShadowPart	shadow;
    KnappPart	knapp;
} KnappRec;

#endif /* KnappP_h */

