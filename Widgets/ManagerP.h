/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ManagerP_h
#define ManagerP_h

#include "Manager.h"
#include <X11/ConstrainP.h>

typedef struct ManagerConstraintsPart {
    Boolean		contain_horiz;
    Boolean		contain_vert;
} ManagerConstraintsPart;

typedef struct ManagerConstraintsRec {
    ManagerConstraintsPart	manager;
} ManagerConstraintsRec, *ManagerCons;    

typedef struct ManagerPart {
    XtCallbackList	resize_callback;
    Dimension		pref_width;
    Dimension		pref_height;
    /* private */
} ManagerPart;

typedef struct ManagerRec {
    CorePart		core;
    CompositePart	composite;
    ConstraintPart	constraint;
    ManagerPart		manager;
} ManagerRec;

typedef struct {
    XtPointer	extension;
} ManagerClassPart;

typedef struct ManagerClassRec {
    CoreClassPart	core_class;
    CompositeClassPart	composite_class;
    ConstraintClassPart	constraint_class;
    ManagerClassPart	manager_class;
} ManagerClassRec;

extern ManagerClassRec managerClassRec;

#endif /* ManagerP_h */
