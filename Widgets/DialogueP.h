/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef DialogueP_h
#define DialogueP_h

#include "Dialogue.h"
#include "CloseShP.h"

typedef struct {
    XtPointer	empty;
} DialogueClassPart;

typedef struct DialogueClassRec {
    CoreClassPart		core_class;
    CompositeClassPart		composite_class;
    ShellClassPart		shell_class;
    WMShellClassPart		wm_shell_class;
    VendorShellClassPart	vendor_shell_class;
    TransientShellClassPart	transient_shell_class;
    CloseShellClassPart		close_shell_class;
    DialogueClassPart		dialogue_class;
} DialogueClassRec;

extern DialogueClassRec dialogueClassRec;

typedef struct {
    XtCallbackList      callback;
    String		message;
    String		buffer;
    String		left_label;
    String		middle_label;
    String		right_label;
    /* private data */
    Widget		layout;
    Widget		message_widget;
    Widget		text_field;
    Widget		left_knapp;
    Widget		middle_knapp;
    Widget		right_knapp;
} DialoguePart;

typedef struct DialogueRec {
    CorePart		core;
    CompositePart	composite;
    ShellPart		shell;
    WMShellPart		wm;
    VendorShellPart	vendor;
    TransientShellPart	transient;
    CloseShellPart	close_shell;
    DialoguePart	dialogue;
} DialogueRec;

#endif /* DialogueP_h */

