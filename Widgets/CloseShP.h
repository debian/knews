/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef CloseShP_h
#define CloseShP_h

#include "CloseSh.h"
#include <X11/ShellP.h>

#define XtInheritCloseWindow	((XtWidgetProc)_XtInherit)

typedef struct {
    XtWidgetProc	close_window;
    XtPointer		empty;
} CloseShellClassPart;

typedef struct CloseShellClassRec {
    CoreClassPart		core_class;
    CompositeClassPart		composite_class;
    ShellClassPart		shell_class;
    WMShellClassPart		wm_shell_class;
    VendorShellClassPart	vendor_shell_class;
    TransientShellClassPart	transient_shell_class;
    CloseShellClassPart		close_shell_class;
} CloseShellClassRec;

extern CloseShellClassRec closeShellClassRec;

typedef struct {
    Cursor		cursor;
    XtCallbackList	close_callback;
} CloseShellPart;

typedef struct CloseShellRec {
    CorePart		core;
    CompositePart	composite;
    ShellPart		shell;
    WMShellPart		wm;
    VendorShellPart	vendor;
    TransientShellPart	transient;
    CloseShellPart	close_shell;
} CloseShellRec;

#endif /* CloseShP_h */

