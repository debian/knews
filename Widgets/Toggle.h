/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Toggle_h
#define Toggle_h

#ifndef XtCToggleSize
#define XtCToggleSize "ToggleSize"
#endif
#ifndef XtCToggleOffset
#define XtCToggleOffset "ToggleOffset"
#endif
#ifndef XtCSet
#define XtCSet "Set"
#endif

#ifndef XtNtoggleSize
#define XtNtoggleSize "toggleSize"
#endif
#ifndef XtNtoggleOffset
#define XtNtoggleOffset "toggleOffset"
#endif
#ifndef XtNtoggleShadowWidth
#define XtNtoggleShadowWidth "toggleShadowWidth"
#endif
#ifndef XtNset
#define XtNset "set"
#endif

typedef struct ToggleClassRec*	ToggleWidgetClass;
typedef struct ToggleRec*	ToggleWidget;

extern WidgetClass toggleWidgetClass;

extern void	ToggleSet(Widget, int);
extern int	ToggleGet(Widget);

#endif /* Toggle_h */
