/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef MessageP_h
#define MessageP_h

#include "Message.h"
#include <X11/CoreP.h>

typedef struct {
    XtPointer	extension;
} MessageClassPart;

typedef struct MessageClassRec {
    CoreClassPart	core_class;
    MessageClassPart	message_class;
} MessageClassRec;

extern MessageClassRec messageClassRec;

typedef struct {
    Pixel		foreground_pixel;
    XFontStruct		*font;
    String		buffer;
    Dimension		internal_height;
    Dimension		internal_width;
    Dimension		pref_chars;
    Boolean		center;
    /* private data */
    Cardinal		rows;
    Cardinal		n_alloc;
    GC			default_gc;
} MessagePart;

typedef struct MessageRec {
    CorePart	core;
    MessagePart	message;
} MessageRec;

#endif /* MessageP_h */
