/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef PullRightP_h
#define PullRightgP_h

#include "PullRight.h"
#include "StringGP.h"

typedef struct {
    XtPointer		extension;
} PullRightGadgetClassPart;

typedef struct PullRightGadgetClassRec {
    RectObjClassPart		rect_class;
    MenuGadgetClassPart		menu_g_class;
    StringGadgetClassPart	string_g_class;
    PullRightGadgetClassPart	pull_right_class;
} PullRightGadgetClassRec;

extern PullRightGadgetClassRec pullRightGadgetClassRec;

typedef struct {
    String	menu_name;
    Dimension	arrow_size;
    Dimension	arrow_offset;
    Dimension	arrow_shadow_width;
    /* private data */
    Widget	menu;
} PullRightGadgetPart;

typedef struct PullRightGadgetRec {
    ObjectPart		object;
    RectObjPart		rectangle;
    MenuGadgetPart	menu_g;
    StringGadgetPart	string_g;
    PullRightGadgetPart	pull_right;
} PullRightGadgetRec;

#endif /* PullRightP_h */
