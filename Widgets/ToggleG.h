/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef ToggleG_h
#define ToggleG_h

#ifndef XtCToggleSize
#define XtCToggleSize "ToggleSize"
#endif
#ifndef XtCToggleOffset
#define XtCToggleOffset "ToggleOffset"
#endif
#ifndef XtCToggleShadowWidth
#define XtCToggleShadowWidth "ToggleShadowWidth"
#endif
#ifndef XtCSet
#define XtCSet "Set"
#endif

#ifndef XtNtoggleSize
#define XtNtoggleSize "toggleSize"
#endif
#ifndef XtNtoggleOffset
#define XtNtoggleOffset "toggleOffset"
#endif
#ifndef XtNtoggleShadowWidth
#define XtNtoggleShadowWidth "toggleShadowWidth"
#endif
#ifndef XtNset
#define XtNset "set"
#endif

typedef struct ToggleGadgetClassRec*  ToggleGadgetClass;
typedef struct ToggleGadgetRec*       ToggleGadget;

extern WidgetClass toggleGadgetClass;

extern void	ToggleGadgetSet(Widget, int);
extern int	ToggleGadgetGet(Widget);

#endif /* ToggleG_h */
