/*
 *  Copyright (C) 1995, 1996  Karl-Johan Johnsson.
 */

#ifndef Sash_h
#define Sash_h

#ifndef XtNcursor
#define XtNcursor "cursor"
#endif
#ifndef XtNpreferredWidth
#define XtNpreferredWidth "preferredWidth"
#endif
#ifndef XtNpreferredHeight
#define XtNpreferredHeight "preferredHeight"
#endif

typedef struct SashClassRec*  SashWidgetClass;
typedef struct SashRec*       SashWidget;

extern WidgetClass sashWidgetClass;

typedef struct {
    XEvent	*event;
    String	*params;
    Cardinal	*no_params;
} SashCallDataRec, *SashCallData;

#endif /* Sash_h */
